# repr(x) -> __repr__
# x() -> __call__

class Polynomial:
    def __init__(self, *coeffs):
        self.coeffs = coeffs

    def __repr__(self):
        return 'Polynomial(*{!r})'.format(self.coeffs)

    def __add__(self, other):
        return Polynomial(*(x + y for x, y in zip(self.coeffs, other.coeffs)))

    def __call__(self):
        pass

p1 = Polynomial(1,2,3) # x^2 + 2x +3
p2 = Polynomial(3,2,3) # 3x^2 + 2x + 3
p3 = Polynomial(4,2,3) # 4x^2 + 2x + 3


# 3 core patterns have to understand in order understand Object oriention in Python
# 1. protocal view of python
# 2. built in inheritance protocol
# 3. how OO works in python 

